grant select, insert, update, delete on DB2ADMIN.AUTHORS to db2user;
grant select, insert, update, delete on DB2ADMIN.BOOKS to db2user;
grant select, insert, update, delete on DB2ADMIN.BOOK_AUTHORS to db2user;
grant select, insert, update, delete on DB2ADMIN.BOOK_GENRES to db2user;
grant select, insert, update, delete on DB2ADMIN.GENRES to db2user;
grant select, insert, update, delete on DB2ADMIN.PUBLISHERS to db2user;

commit;