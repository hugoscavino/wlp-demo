package org.scavino;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet(urlPatterns="/db2")
public class DB2Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final int NUMBER_OF_CONNECTION_RETRIES = 3;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InitialContext initialContext = null;
        Connection connection = null;
        DataSource dataSource = null;
        String schemaName = "ERROR";
        try {
            initialContext = new InitialContext();
            if (initialContext != null){
                dataSource = (DataSource)initialContext.lookup("jdbc/db2");
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }

        if (dataSource != null){
            for (int i = 0; i < NUMBER_OF_CONNECTION_RETRIES ; i++) {
                try {
                    connection = dataSource.getConnection();
                    schemaName = connection.getSchema();
                    //STEP 4: Execute a query
                    System.out.println("Creating statement...");
                    Statement stmt = connection.createStatement();

                    System.out.println("Connected database schema " + schemaName + " successfully...");
                    response.getWriter().append("Hello! Today is " + LocalDateTime.now() + " and connecting to SCHEMA " + schemaName);

                    String sql = "SELECT AUTHOR_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME FROM DB2ADMIN.AUTHORS";
                    ResultSet rs = stmt.executeQuery(sql);

                    //STEP 5: Extract data from result set
                    while(rs.next()){
                        //Retrieve by column name
                        int id  = rs.getInt("AUTHOR_ID");
                        String first = rs.getString("FIRST_NAME");
                        String middle = rs.getString("MIDDLE_NAME");
                        String last = rs.getString("LAST_NAME");

                        //Display values
                        System.out.print("ID: " + id);
                        System.out.print(", First: " + first);
                        System.out.println(", Last: " + last);

                        response.getWriter().append("Authors : " + id + " : " + first + " " + last);

                    }
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}