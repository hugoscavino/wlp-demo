# Liberty Maven Plugin Demo
#### Open Liberty and Liberty maven plugin sample application

##Summary
WLP (Liberty Server) demo application demonstrating the uasage of server.xml and pom.xml to 
build, deploy the WAR and EAR using the maven plugin `liberty-maven-plugin`

#### Version 1.0.0

## How do I get set up? ###

### Step 1
#### Download WLP (Liberty) J2EE Software
This version is Java EE8 compliant (preferred). Let's assume you installed this server at 

`D:\Apps\wlp-webProfile8-20.0.0.4\wlp`

[wlp Liberty java ee8](https://developer.ibm.com/wasdev/downloads/#asset/runtimes-wlp-javaee8)

#### Download Location for WLP (Liberty) Web Software
THis one is just the web server version and cannot be expanded with the admin console or other features

[wlp webProfile8 only](https://developer.ibm.com/wasdev/downloads/#asset/runtimes-wlp-webProfile8)

### Step 2
Create a windows environment variable for the installation directory where you installed Liberty from above

`WLP_HOME` should reference you installation directory for example `D:\Apps\wlp-webProfile8-20.0.0.4\wlp`

Optionally add `%WLP_HOME%\bin` to your windows PATH so you can run the utilities from anywhere

### Step 3 Copy DB2 libs to the ${shared}
[IBM Doc](https://www.ibm.com/support/knowledgecenter/SSEQTP_liberty/com.ibm.websphere.wlp.doc/ae/cwlp_sharedlibrary.html)

You can place global libraries in two locations, shared is preferred:
* ${shared.config.dir}/lib/global -- preferred
* ${server.config.dir}/lib/global

e.g. -- D:\Apps\wlp-javaee8-20.0.0.4\wlp\usr\shared\config\lib\global

The two libraries are the DB2 JDBC 4.x.x driver and the IBM licence
* db2jcc4.jar
* db2jcc_license_cu.jar

### Step 4 Application Assumes there is DB2 Customer Table
The table is CUSTOMER with the following DDL
```sql
CREATE TABLE "DEMO"."CUSTOMER" (
 		"id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE -2147483648 MAXVALUE 2147483647 NO CYCLE CACHE 20 NO ORDER ), 
 		"name" VARCHAR(255 OCTETS), 
 		"email" VARCHAR(255 OCTETS), 
 		"address" VARCHAR(512 OCTETS)
)
```

### Step 5 Clone this repository
[bitbucket.org/hugoscavino/wlp-demo/](https://bitbucket.org/hugoscavino/wlp-demo/src/master/)

### Step 6 run the mvn and clean commands
Run mvn:clean and then mvn:install to make sure the project compiles. This project was using
Amazon's JDK 11 if you are using 1.8 then make adjustments

### Step 6.5 create mvn short cuts
For each project create maven run configurations to perform the following mvn commands for each module
* liberty:dev  -- start the server in dev mode
* liberty:stop  -- stop the server
* liberty:clean  -- cleans all the logs and dropins, apps

### Step 7 start the dev server
* liberty:dev -- deploys the artifact in the dev server - WAR or EAR

Only a servlet no JDBC nor Spring
* [Servlet Sample](http://localhost:9080/simple-servlet/servlet)

Only a servlet no JDBC nor Spring
* [DB2 Servlet Sample](http://localhost:9080/simple-db-servlet/db2)

Spring 5.0 EAR (not Spring boot) JPA Sample requires DB2
* [simple-wlp-spring-app](https://localhost:9443/simple-wlp-spring-app/)


# Liberty Plugin Documentation
https://github.com/OpenLiberty/ci.maven

## Common Liberty Plugin configuration Parameters
[common-parameters](https://github.com/OpenLiberty/ci.maven/blob/master/docs/common-parameters.md#common-parameters)

# Admin and Configuration
## Liberty Administrative Center ("Admin Center")
[Liberty Administrative Center](https://www.ibm.com/support/knowledgecenter/SSEQTP_liberty/com.ibm.websphere.wlp.doc/ae/twlp_ui.html) 

## Optional - Admin Center
### Install adminCenter-1.0 feature
In order to view and manage the server with a UI perform the following
For Version 8.5.5.6 or later, run the installUtility command.
From the `%WLP_HOME%/bin` run :

`installUtility install adminCenter-1.0`

Then traverse to adminCenter to manage your server
`https://localhost:9443/adminCenter/`

# Database Driver Configuration and Testing

* Configuring Driver in Liberty
    * [Configuring DS](https://www.ibm.com/support/knowledgecenter/SSEQTP_liberty/com.ibm.websphere.wlp.doc/ae/twlp_dep_configuring_ds.html)

*  Testing Database Connections
    * [testing-database-connections](https://openliberty.io/blog/2019/09/13/testing-database-connections-REST-APIs.html)

* API to view DataSource Details from Server
    * `https://localhost:9443/ibm/api/config/dataSource`

* API to validate your DataSource
    * `https://localhost:9443/ibm/api/validation/dataSource/DefaultDataSource`

## Adding Global Jars to Liberty
* [wlp shared library](https://www.ibm.com/support/knowledgecenter/SSEQTP_liberty/com.ibm.websphere.wlp.doc/ae/cwlp_sharedlibrary.html)

### Who do I talk to? ###
* Repo owner