<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="scavino.org">

    <title>Customer Demo</title>

    <!-- Bootstrap core CSS -->
    <link href='<spring:url value="/resources/css/bootstrap.min.css"/>' rel="stylesheet" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href='<spring:url value="/resources/assets/css/ie10-viewport-bug-workaround.css"/>' rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href='<spring:url value="/resources/css/grid.css"/>' rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src='<spring:url value="/resources/assets/js/ie-emulation-modes-warning.js" />'></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
    <div class="container">

      <h1>Edit Customer - ${customer.id}</h1>
      <p class="lead">Simple CRUD Demo for Customers connecting to a DB2 Database</p>
		<form:form action="save" method="post" modelAttribute="customer">
		      <form:hidden path="id"/>
		     <div class="row text-left">
		        <div class="col-lg-3" style="padding-bottom:1.6rem">Name:</div>
		        <div class="col-lg-9">
		            <form:input style="min-width: 100%;" path="name" />
		         </div>
		    </div>
            <div class="row text-left">
                <div class="col-lg-3" style="padding-bottom:1.6rem">email:</div>
                <div class="col-lg-9">
                    <form:input style="min-width: 100%;" path="email"/>
                 </div>
            </div>
            <div class="row text-left">
                <div class="col-lg-3" style="padding-bottom:1.6rem">address:</div>
                <div class="col-lg-9">
                    <form:input style="min-width: 100%;" path="address"/>
                 </div>
            </div>
            <div class="row text-center">
                <div class="">
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}">Home</a>
                    <input class="btn btn-primary" type="submit" value="Update">
                 </div>
            </div>
		</form:form>
          <div class="row">
                <div class="text-center">
                </div>
           </div>
	</div>
</body>
</html>