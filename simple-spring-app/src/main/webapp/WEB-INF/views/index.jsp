<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="scavino.org">

    <title>Customer Demo</title>

    <!-- Bootstrap core CSS -->
    <link href='<spring:url value="/resources/css/bootstrap.min.css"/>' rel="stylesheet" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href='<spring:url value="/resources/assets/css/ie10-viewport-bug-workaround.css"/>' rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href='<spring:url value="/resources/css/grid.css"/>' rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src='<spring:url value="/resources/assets/js/ie-emulation-modes-warning.js" />'></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
    <div class="container">

      <h1>Customer Manager</h1>
      <p class="lead">Simple CRUD Demo for Customers connecting to a DB2 Database</p>
        <h3><a href="${pageContext.request.contextPath}/new">Add New Customer +</a></h3>
        <hr>
        <h3>Search for Customers</a></h3>
        <form method="get" action="${pageContext.request.contextPath}/search">
            <div class="row">
                <div class="col-lg-12 text-left">
                    <div class="col-lg-9">
                       <input ="text" name="keyword" style="min-width:100%;padding-bottom:0.9rem"/>
                    </div>
                    <div class="col-lg-3 text-center">
                        <input class="btn btn-primary" type="submit" value="Search" />
                     </div>
                 </div>
             </div>
        </form>
      <hr>
      <div class="row">
        <div class="col-lg-1 text-left" style="font-weight: 700;">ID</div>
        <div class="col-lg-3 text-left" style="font-weight: 700;">Name</div>
        <div class="col-lg-2 text-left" style="font-weight: 700;">e-mail</div>
        <div class="col-lg-3 text-left" style="font-weight: 700;">Address</div>
        <div class="col-lg-3 text-center" style="font-weight: 700;">Action</div>
      </div>
        <c:forEach items="${listCustomer}" var="customer">
            <div class="row">
              <div class="col-lg-1 text-left" style="padding-bottom:2.4rem">${customer.id}</div>
              <div class="col-lg-3 text-left" style="padding-bottom:2.4rem">${customer.name}</div>
              <div class="col-lg-2 text-left" style="padding-bottom:2.4rem">${customer.email}</div>
              <div class="col-lg-3 text-left" style="padding-bottom:2.4rem">${customer.address}</div>
              <div class="col-lg-3 text-center">
                <div class="text-center">
                    <span style="margin-right:15px">
                        <a class="btn btn-primary" href="${pageContext.request.contextPath}/edit?id=${customer.id}">Edit</a>
                    </span>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/delete?id=${customer.id}">Delete</a>
                 </div>
              </div>
            </div>
        </c:forEach>
      <hr>
    </div>
  </body>
</html>