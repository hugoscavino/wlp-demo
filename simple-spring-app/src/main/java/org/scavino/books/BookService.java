package org.scavino.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookService {
    
    @Autowired
    private BookRepository repo;

    public void save(Authors Authors) {
        repo.save(Authors);
    }

    public List<Authors> listAll() {
        return (List<Authors>) repo.findAll();
    }

    public Authors get(Long id) {
        return repo.findById(id).get();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<Authors> search(String keyword) {
        return repo.search(keyword);
    }
}
