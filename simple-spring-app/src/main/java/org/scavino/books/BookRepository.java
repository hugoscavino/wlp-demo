package org.scavino.books;

import org.scavino.customer.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository  extends CrudRepository<Authors, Long> {

    @Query(value = "SELECT c FROM Authors c")
    public List<Authors> search(@Param("keyword") String keyword);
}