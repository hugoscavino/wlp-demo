package org.scavino.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller()
@RequestMapping("/authors")
public class BooksController {

    @Autowired
    private BookService service;

    @RequestMapping("/")
    public ModelAndView home() {
        List<Authors> listAuthors = service.listAll();
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("listAuthors", listAuthors);
        return mav;
    }

    @RequestMapping("/new")
    public String newAuthorsForm(Map<String, Object> model) {
        Authors Authors = new Authors();
        model.put("Authors", Authors);
        return "new_Authors";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveAuthors(@ModelAttribute("Authors") Authors Authors) {
        service.save(Authors);
        return "redirect:/";
    }

    @RequestMapping("/edit")
    public ModelAndView editAuthorsForm(@RequestParam long id) {
        ModelAndView mav = new ModelAndView("edit_Authors");
        Authors Authors = service.get(id);
        mav.addObject("Authors", Authors);

        return mav;
    }

    @RequestMapping("/delete")
    public String deleteAuthorsForm(@RequestParam long id) {
        service.delete(id);
        return "redirect:/authors";
    }

    @RequestMapping("/search")
    public ModelAndView search(@RequestParam String keyword) {
        List<Authors> result = service.search(keyword);
        ModelAndView mav = new ModelAndView("search");
        mav.addObject("result", result);

        return mav;
    }
}