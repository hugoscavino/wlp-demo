package org.scavino.books;
import javax.persistence.*;

/**
 * The "\ had to be added for DB2 as the column
 * names had to be escaped for some reason.
 * Adding the Dialect did not seem to make a difference
 *
 * Hibernate reference guide
 *   5.4. SQL quoted identifiers
 *
 * JPA 2.0 specification
 *   2.13 Naming of Database Objects
 */
@Entity
@Table(name = "AUTHORS", schema = "DB2ADMIN")
public class Authors {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"AUTHOR_ID\"")
    private Long id;

    @Column(name = "\"FIRST_NAME\"")
    private String first;

    @Column(name = "\"MIDDLE_NAME\"")
    private String middle;

    @Column(name = "\"LAST_NAME\"")
    private String last;
}
