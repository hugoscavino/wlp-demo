package org.scavino.customer;

import javax.persistence.*;

/**
 * The "\ had to be added for DB2 as the column
 * names had to be escaped for some reason.
 * Adding the Dialect did not seem to make a difference
 *
 * Hibernate reference guide
 *   5.4. SQL quoted identifiers
 *
 * JPA 2.0 specification
 *   2.13 Naming of Database Objects
 */
@Entity
@Table(name = "CUSTOMER", schema = "HUGO")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Long id;
	private String fullname;
	private String email;
	private String mailaddress;

	protected Customer() {
	}

	protected Customer(final String fullname, final String email, final String mailaddress) {
		this.fullname = fullname;
		this.email = email;
		this.mailaddress = mailaddress;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return fullname;
	}

	public void setName(String name) {
		this.fullname = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return mailaddress;
	}

	public void setAddress(String address) {
		this.mailaddress = address;
	}

}
